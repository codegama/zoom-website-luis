 <form  action="{{route('admin.users.index')}}" method="GET" role="search">

    <div class="row pt-2 pb-2">

        <div class="col-2">
            @if(Request::has('search_key'))
                <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
            @endif
        </div>
        
        <div class="col-3">

            <select class="form-control select2" name="status">

                <option  class="select-color" value="">{{tr('select_status')}}</option>

                <option  class="select-color" value="{{USER_APPROVED}}" @if(Request::get('status') == USER_APPROVED && Request::get('status')!='') selected @endif>{{tr('approved')}}</option>

                <option  class="select-color" value="{{USER_DECLINED}}" @if(Request::get('status') == USER_DECLINED && Request::get('status')!='') selected @endif>{{tr('declined')}}</option>

            </select>

        </div>

        <div class="col-7">

            <div class="input-group">
                <input type="text" class="form-control" name="search_key"
                    placeholder="{{tr('users_search_placeholder')}}"> <span class="input-group-btn">
                    &nbsp

                    <button type="submit" class="btn btn-primary btn-width">
                       {{tr('search')}}
                    </button>

                    <a class="btn btn-primary" href="{{route('admin.users.index')}}">{{tr('clear')}}
                    </a>
                </span>
            </div>

        </div>

    </div>

</form>